<?php
namespace app\study\controller;

use app\BaseController;

class Curl extends BaseController
{
    /**
     * 一个简单的网页爬虫
     * @return [type] [description]
     */
    public function index()
    {
        // 初始化
        $curl = curl_init('http://www.baidu.com');
        // dump($curl);exit;
        // halt($curl);
        // 执行
        curl_exec($curl);
        curl_close($curl);
    }

    /**
     * 代码实例-PHP-cURL实战
     * 实例描述：在网络上下载一个网页并把内容中的“百度”替换为“屌丝”之后输出
     * @return [type] [description]
     */
    public function index2()
    {
        $curlObj = curl_init();
        // curl_setopt() 为给定的cURL会话句柄设置一个选项。
        // CURLOPT_URL 需要获取的URL地址，也可以在curl_init()函数中设置。 string值
        // 设置访问网页的URL
        curl_setopt($curlObj, CURLOPT_URL, "http://www.baidu.com");
        // CURLOPT_RETURNTRANSFER
        // 将curl_exec()获取的信息以文件流的形式返回，而不是直接输出。
        // 执行之后不直接打印出来
        curl_setopt($curlObj, CURLOPT_RETURNTRANSFER, true);
        $output = curl_exec($curlObj); // 执行
        // halt($output);
        curl_close($curlObj); // 关闭cURL
        echo str_replace("百度", "屌丝", $output);
    }

    /**
     * 用cURL调用WebService获取天气信息
     * @return [type] [description]
     */
    public function weather()
    {
        // $data = 'theCityName=北京';
        $data = 'theCityCode=湛江&theUserID=012666efdd384fa7a7b3993f84111a44';
        // http://ws.webxml.com.cn 垃圾地方 TMD的还收钱
        // 如果有一天不行了就是会员试用期过了
        $curlObj = curl_init();
        curl_setopt_array($curlObj, [
            // 'CURLOPT_URL' => 'http://www.webxml.com.cn/WebServices/WeatherWebService.asmx/getWeatherbyCityName',
            CURLOPT_URL => 'http://ws.webxml.com.cn/WebServices/WeatherWS.asmx/getWeather',
            // 启用时会将头文件的信息作为数据流输出。
            CURLOPT_HEADER => 0,
            CURLOPT_RETURNTRANSFER => 1,
            // 启用时会发送一个常规的POST请求，类型为：application/x-www-form-urlencoded，就像表单提交的一样。
            CURLOPT_POST => 1,
            // POST发送的字段组
            CURLOPT_POSTFIELDS => $data,
            // 一个用来设置HTTP头字段的数组。使用如下的形式的数组进行设置： array('Content-type: text/plain', 'Content-length: 100')
            CURLOPT_HTTPHEADER => [
                "application/x-www-form-urlencodeed;charset=utf-8",
                'Content-length:'.strlen($data)
            ]
        ]);

        $rtn = curl_exec($curlObj);
        // curl_errno() 返回最后一次的错误号
        if( !curl_errno($curlObj) ){
            // $info = curl_getinfo($curlObj);
            // dump($info);
            halt($rtn);
            // echo $rtn;
        }else{
            // curl_error() 返回一个保护当前会话最近一次错误的字符串
            echo 'Curl error:' . curl_error($curlObj);
        }
    }

    /**
     * 代码实例：PHP-cURL实战
     * 实例描述：登录慕课网并下载个人空间页面
     * @return [type] [description]
     */
    public function demo()
    {
        // $data = 'username=demo_peter@126.com&password=123qwe&remember=1';
        // 旧参数不对 ，要按照最新的来
        $data = 'account=dxing1202&password=LDXdx1202&remember=1';
        $curlObj = curl_init(); // 初始化

        // date_default_timezone_set('PRC'); // 使用Cookie时，必须先设置时区
        curl_setopt_array($curlObj,[
            // 设置访问页面的URL
            // CURLOPT_URL => 'http://www.imooc.com/user/login',
            // CURLOPT_URL => 'https://www.imooc.com/user/newlogin',
            CURLOPT_URL => 'https://www.kancloud.cn/auth/login',
            // 执行之后不直接打印出来
            CURLOPT_RETURNTRANSFER => true,
            // Cookie相关设置，这部分设置需要在所有回话开始之前设置
            // 启用时curl会仅仅传递一个session cookie，忽略其他的cookie，默认状况下cURL会将所有的cookie返回给服务端。
            // session cookie是指那些用来判断服务器端的session是否有效而存在的cookie。
            CURLOPT_COOKIESESSION => true,
            // 包含cookie数据的文件名，cookie文件的格式可以是Netscape格式，或者只是纯HTTP头部信息存入文件。
            CURLOPT_COOKIEFILE => 'cookiefile',
            // 连接结束后保存cookie信息的文件。
            CURLOPT_COOKIEJAR => 'cookiefile',
            // 设定HTTP请求中"Cookie: "部分的内容。多个cookie用分号分隔，分号后带一个空格(例如， "fruit=apple; colour=red")。
            CURLOPT_COOKIE => session_name() . '=' . session_id(),
            // 启用时会将头文件的信息作为数据流输出。
            CURLOPT_HEADER => false,
            // 这样能够让cURL支持页面链接跳转
            // 启用时会将服务器服务器返回的"Location: "放在header中递归的返回给服务器，
            // 使用CURLOPT_MAXREDIRS可以限定递归返回的数量。
            CURLOPT_FOLLOWLOCATION => true,
            // 启用时会发送一个常规的POST请求，类型为：application/x-www-form-urlencoded，就像表单提交的一样。
            CURLOPT_POST => true,
            // POST发送的字段组
            CURLOPT_POSTFIELDS => $data,
            // 一个用来设置HTTP头字段的数组。使用如下的形式的数组进行设置： array('Content-type: text/plain', 'Content-length
            CURLOPT_HTTPHEADER => [
                'application/x-www-form-urlencoded;charset=utf-8',
                'Content-length: ' . strlen($data)
            ]
        ]);
        curl_exec($curlObj); // 执行
        // curl_setopt($curlObj, CURLOPT_URL, 'https://www.imooc.com/u/index/courses');
        curl_setopt($curlObj, CURLOPT_URL, 'https://www.kancloud.cn/dashboard');
        curl_setopt($curlObj, CURLOPT_POST, false);
        curl_setopt($curlObj, CURLOPT_HTTPHEADER, [
            'Content-type: text/xml'
        ]);
        $output = curl_exec($curlObj); // 执行
        curl_close($curlObj); // 关闭cURL
        echo $output;
    }

    /**
     * 代码实例：PHP-cURL实战
     * 实例描述：从 FTP服务器 下载一个文件到本地
     * @return [type] [description]
     */
    public function ftpDownload()
    {
        // sets up the output file 设置输出文件
        // 打开方式 wb写入
        // "w" （写入方式打开，清除文件内容，如果文件不存在则尝试创建之）
        // $outfile = fopen('dest.txt','wb'); // 保存到本地的文件名
        $outfile = fopen('dest.txt','w'); // 保存到本地的文件名

        $curlObj = curl_init();
        curl_setopt_array($curlObj,[
            // CURLOPT_URL => 'ftp://192.168.1.100/downloaddemo.txt',
            // 加上ftp 默认都是端口21 可以不用填写
            CURLOPT_URL => 'ftp://122.152.224.138/demo.txt',
            CURLOPT_HEADER => false,
            CURLOPT_RETURNTRANSFER => true,
            // 设置cURL允许执行的最长秒数。
            CURLOPT_TIMEOUT => 300, // times out atfer 300s
            // 传递一个连接中需要的用户名和密码，格式为："[username]:[password]"。
            // FTP用户名 : 密码
            // CURLOPT_USERPWD => 'peter.zhou:123456',
            CURLOPT_USERPWD => 'study:Ri7NjWtGaePrGjbp',
            // 设置输出文件的位置，值是一个资源类型，默认为STDOUT (浏览器)。
            // value应该被设置一个流资源 （例如使用fopen()）
            CURLOPT_FILE => $outfile
        ]);
        $rtn = curl_exec($curlObj);
        // fclose() 函数关闭打开的文件。
        fclose($outfile);
        if( !curl_errno($curlObj) ){
            // $info = curl_getinfo($curlObj);
            // print_r($info);
            echo "RETURN: " . $rtn;
        }else{
            // curl_error() 返回一个保护当前会话最近一次错误的字符串
            echo 'Curl error:' . curl_error($curlObj);
        }

        curl_close($curlObj);
    }

    /**
     * 代码实例：PHP-cURL实战
     * 实例描述：把本地文件上传到FTP服务器上
     * @return [type] [description]
     */
    public function ftpUpload()
    {
        $localfile = 'ftp01.php';
        // "r" （只读方式打开，将文件指针指向文件头）
        $fp = fopen($localfile,'r'); // 保存到本地的文件名

        $curlObj = curl_init();
        curl_setopt_array($curlObj,[
            // 加上ftp 默认都是端口21 可以不用填写
            // 保存到哪个文件 如果文件不存在会自动创建 存在会覆盖原文件
            CURLOPT_URL => 'ftp://122.152.224.138/ftp01_uploaded.php',
            CURLOPT_HEADER => false,
            CURLOPT_RETURNTRANSFER => true,
            // 设置cURL允许执行的最长秒数。
            CURLOPT_TIMEOUT => 300, // times out atfer 300s
            // 传递一个连接中需要的用户名和密码，格式为："[username]:[password]"。
            // FTP用户名 : 密码
            // CURLOPT_USERPWD => 'peter.zhou:123456',
            CURLOPT_USERPWD => 'study:Ri7NjWtGaePrGjbp',
            // 启用后允许文件上传。
            CURLOPT_UPLOAD => true,
            // 在上传文件的时候需要读取的文件地址，值是一个资源类型。
            CURLOPT_INFILE => $fp,
            // 设定上传文件的大小限制，字节(byte)为单位。
            // filesize() 函数返回指定文件的大小。该函数返回文件大小的字节数。
            CURLOPT_INFILESIZE => filesize($localfile)
        ]);
        $rtn = curl_exec($curlObj);
        // fclose() 函数关闭打开的文件。
        fclose($fp);
        if( !curl_errno($curlObj) ){
            // $info = curl_getinfo($curlObj);
            // print_r($info);
            echo "Uploaded successfully.";
        }else{
            // curl_error() 返回一个保护当前会话最近一次错误的字符串
            echo 'Curl error:' . curl_error($curlObj);
        }

        curl_close($curlObj);
    }

    /**
     * 代码实例：PHP-cURL实战
     * 实例描述：下载网络上面的一个HTTPS的资源
     * @return [type] [description]
     */
    public function https()
    {
        // 使用Cookie时，必须先设置时区
        // 访问https的时候还是把时区加上
        date_default_timezone_set('PRC');
        $curlObj = curl_init();
        curl_setopt_array($curlObj,[
            // 设置访问网页的URL
            // js文件
            CURLOPT_URL => 'https://ajax.aspnetcdn.com/ajax/jquery.validate/1.12.0/jquery.validate.js',
            // 执行之后不直接打印出来
            CURLOPT_RETURNTRANSFER => true,
            // 设置HTTPS支持
            // 禁用后cURL将终止从服务端进行验证。
            CURLOPT_SSL_VERIFYPEER => false
        ]);
        // 执行
        $output = curl_exec($curlObj);
        // 关闭cURL
        curl_close($curlObj);

        echo $output;
    }
}
