//注意：导航 依赖 element 模块，否则无法进行功能性操作
layui.use(['element','jquery'], function(){
    var element = layui.element,
    $ = layui.jquery;

    // Swiper 轮播图
    var mySwiper = new Swiper ('.swiper-container', {
        // direction: 'vertical', // 垂直切换选项
        loop: true, // 循环模式选项
        dynamicBullets: true,
        // 自动切换
        autoplay:true,
        // autoplay: {
        //     delay: 3000,
        //     stopOnLastSlide: false,
        //     disableOnInteraction: true,
        // }

        // 如果需要分页器
        pagination: {
          el: '.swiper-pagination',
        },

        // 如果需要前进后退按钮
        navigation: {
          nextEl: '.swiper-button-next',
          prevEl: '.swiper-button-prev',
        },

        // 如果需要滚动条
        // scrollbar: {
        //   el: '.swiper-scrollbar',
        // },
    })


    $(".layui-card-body").hover(function() {
            // $(this).css({'border':'2px solid #66B976'});
            // $(this).addClass('layui-anim layui-anim-scale');
            $(this).children('div').children('a').css({'color':'#66B976','font-size':'16px'});
            $(this).children('div').children('p').css({'color':'#2F4056','font-size':'14px'});
            $(this).children('.card-movie-info-block-focus').css({'background':'#66B976','opacity':'0.9'});
            $(this).children('.card-movie-info-block-lang').css({'background':'#66B976','opacity':'0.9'});
            $(this).children('.card-movie-info-block-subtitle').css({'background':'#66B976','opacity':'0.9'});
            $(this).children('.card-movie-info-block-class').removeClass('layui-bg-blue');
            $(this).children('.card-movie-info-block-class').css({'opacity':'0.9'});
            $(this).children('.card-movie-info-block-rating').css({'color':'red','opacity':'0.9'});
        },
        function() {
            $(this).children('div').children('a').removeAttr("style");
            $(this).children('div').children('p').removeAttr("style");
            $(this).children('.card-movie-info-block-focus').removeAttr('style');
            $(this).children('.card-movie-info-block-lang').removeAttr('style');
            $(this).children('.card-movie-info-block-subtitle').removeAttr('style');
            $(this).children('.card-movie-info-block-class').addClass('layui-bg-blue');
            $(this).children('.card-movie-info-block-class').css({'opacity':'0.8'});
            $(this).children('.card-movie-info-block-rating').css({'color':''});
        }
    );


});
